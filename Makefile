.PHONY: all protos

all: protos

protos:
	protoc --twirp_out=. --go_out=. ./*.proto
