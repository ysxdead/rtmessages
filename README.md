# RTMessages

Protobuf message definitions for communication between RotCore services.

### Compiling protocol buffers

Obtain:

`https://github.com/protocolbuffers/protobuf`

`https://github.com/golang/protobuf`

`github.com/twitchtv/twirp/protoc-gen-twirp`

`make`

### Using in Go code

`import "bitbucket.org/izzymg/rtmessages"`